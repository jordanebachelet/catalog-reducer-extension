Catalog Reducer Extension
=================

--------------------------

Introduction
------------
The Catalog Reducer Extension is a Business Manager Extension that was created to provide a simple, easy way to export smaller version of Production catalogs, for use on sandboxes on the Demandware Platform.

Documentation
----------------
Please reference the [Catalog Reducer Extension documentation](https://bitbucket.org/demandware/catalog-reducer-extension/wiki/Home) for detailed descriptions for installation and usage.

Support / Contributing
----------------------
Feel free to create issues and enhancement requests or discuss/comment on existing issues/requests. This will help us understand which area, or component has the biggest need.

Release History
---------------
May 6, 2015 - version 0.7.13

Creators:
--------------- 
Project Lead: Andrew Tougas: atougas@demandware.com

Tech Lead: Rob Turner: rturner@demandware.com

Code Contributors:  
Tony Ruschioni: truschioni@demandware.com  
Elec Boothe: jboothe@demandware.com  
Ramasree Pitla: rpitla@demandware.com